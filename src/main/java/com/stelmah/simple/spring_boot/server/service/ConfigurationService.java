package com.stelmah.simple.spring_boot.server.service;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class ConfigurationService {

    private static final String SERVER_LOGINS_FORMAT = "servers.%s.logins";
    @Getter
    private final Map<String, String> loginMap = new HashMap<>();
    private final Map<String, String> urlMap = new HashMap<>();
    private final Environment env;
    @Value("${servers.names}")
    private String[] serverNames;
    @Value("${servers.urls}")
    private String[] serverUrls;

    @PostConstruct
    public void init() {
        if (serverNames.length != serverUrls.length) {
            log.error("Server names size({}) != server urls size ({})", serverNames.length, serverUrls.length);
            throw new IllegalStateException("Invalid arrays size");
        }
        for (int i = 0; i < serverNames.length; i++) {
            urlMap.put(serverNames[i], serverUrls[i]);
            if (env.containsProperty(String.format(SERVER_LOGINS_FORMAT, serverNames[i]))) {
                String[] logins = env.getProperty(String.format(SERVER_LOGINS_FORMAT, serverNames[i]), String[].class);
                if (logins != null) {
                    for (int j = 0; j < logins.length; j++) {
                        if (loginMap.containsKey(logins[j])) {
                            log.error("Double times used login: {}", logins[j]);
                        } else {
                            loginMap.put(logins[j], serverUrls[i]);
                        }
                    }
                }
            }
        }
    }
}
