package com.stelmah.simple.spring_boot.server.controller;

import com.stelmah.simple.spring_boot.server.service.ConfigurationService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class ConfigurationController {

    private final ConfigurationService configurationService;

    @PostMapping("/rest/server")
    public String getServerByLogin(@RequestBody String login) {
        return configurationService.getLoginMap().getOrDefault(login, null);
    }
}
