package com.stelmah.simple.spring_boot.server.configuration;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@org.springframework.context.annotation.Configuration
@ComponentScan({"com.stelmah.simple.spring_boot.server"})
@PropertySource("file:server-data.properties")
public class Configuration {

}
